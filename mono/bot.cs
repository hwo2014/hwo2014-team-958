using System;
using System.IO;
using System.Net.Sockets;
using System.Collections.Generic;
using Newtonsoft.Json;

public class Bot {
	public static void Main(string[] args) {
	    string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];
        string trackName = null;
        if (args.Length == 5)
        {
            trackName = args[4];
        }

		Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		using(TcpClient client = new TcpClient(host, port)) {
			NetworkStream stream = client.GetStream();
			StreamReader reader = new StreamReader(stream);
			StreamWriter writer = new StreamWriter(stream);
			writer.AutoFlush = true;

            if (trackName != null)
            {
                new Bot(reader, writer, new CreateRace(trackName, null, null, botName, botKey), new JoinServer(trackName, null, null, botName, botKey));
            }
            else
            {
                new Bot(reader, writer, new Join(botName, botKey));
            }
		}
	}
    int gameTick;

    private List<Piece> pieceArray;

    private bool msgSent;

    private bool movedToCoreSide;
    private bool requestedToSwitch;
    private int crashCount;
    private int switchCount;
    //radius 50 works at 467217
    private const float baseThrottle = .45f;
    private float throttleAim;
    private float sendThrottle;
    private int numLanes;
    private int? currentLane;

    private int curve;

    private int lapNum;
    private int lapCount;
    private int? previousLane;
    private int previousPiece;
    private int currentPiece;
    private float previousPieceLocation;
    private float currentPieceLocation;
    private float[] pieceAccum;
    private float currentAngle;
    private float currentSpeed;
    
    private bool turbo;
    private float turboDuration;
    private int turboTicks;
    private float turboMultiplier;

    private List<Piece> nextSeg;
    private Direction generalDirection;
    private Direction subDirection;
    JsonTextReader jsonReader;


	private StreamWriter writer;

	Bot(StreamReader reader, StreamWriter writer, CreateRace create, JoinServer join) {
        this.writer = writer;
        send(create);
        send(join);

        BotMain(reader, writer);
    }
    Bot(StreamReader reader, StreamWriter writer, Join join)
    {
        this.writer = writer;
        send(join);

        BotMain(reader, writer);
    } 
    void BotMain(StreamReader reader, StreamWriter writer)
    {
		string line;

        gameTick = 0;

        currentLane = null;
        currentPiece = 0;
        currentPieceLocation = 0;
        crashCount = 0;
        switchCount = 0;
        requestedToSwitch = false;
        movedToCoreSide = false;
        lapCount = 1;
        //inCurve = false;
        //nearCurve = false;
        //highestSpeed = 0;
        
		while((line = reader.ReadLine()) != null) {
			MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
			switch(msg.msgType) {
				case "carPositions":
                    this.gameTick++;
                    msgSent = false;
                    GetCarInfo(msg);
                    //Lane Switching Segment
                    if (!movedToCoreSide && currentLane.HasValue)
                    {
                        if (currentLane != GetLaneToAim(generalDirection))
                        {
                            send(new SwitchLane(generalDirection));
                        }
                        if (currentLane + 1 == GetLaneToAim(generalDirection))
                        {
                            movedToCoreSide = true;
                        }
                    }
                    else if (pieceArray[currentPiece].LaneSwitch && !requestedToSwitch)
                    {
                        requestedToSwitch = true;
                        nextSeg = GetSubTrackList(currentPiece);
                        subDirection = GetDirectionToAim(nextSeg);
                        if (currentLane.HasValue)
                        {
                            if (currentLane != GetLaneToAim(subDirection))
                            {
                                if (!msgSent)
                                {
                                    msgSent = true;
                                    send(new SwitchLane(subDirection));
                                    switchCount++;
                                }
                            }
                        }
                    }
                    else if (pieceArray[NumWrapper.RoundNumber(currentPiece - 1, pieceArray.Count)].LaneSwitch && requestedToSwitch)
                    {
                        requestedToSwitch = false;
                    }
                    //IO
                    Console.Clear();
                    Console.WriteLine("Current Piece Index is: " + currentPiece.ToString());
                    Console.WriteLine("Lap count is " + lapCount + " out of " + lapNum);
                    Console.WriteLine("Current Lane number is " + currentLane.ToString());
                    Console.WriteLine("Lane to go to is " + GetLaneToAim(subDirection));
                    Console.WriteLine("Current piece curves " + pieceArray[currentPiece].Angle);
                    Console.WriteLine("The radius of said curve is " + pieceArray[currentPiece].Radius);
                    Console.WriteLine("Best lane to be in is " + GetDirectionToAim(nextSeg));
                    Console.WriteLine("Current Angle is " + currentAngle.ToString());
                    Console.WriteLine("Switch Count is " + switchCount.ToString());
                    Console.WriteLine("The statement we have requested to switch is " + requestedToSwitch);
                    Console.WriteLine(currentPiece.ToString() + " " + GetNextCurve(currentPiece));
                    /*if (curve == currentPiece)
                    {
                        highestSpeed = 0;*/
                        curve = GetNextCurve(this.currentPiece);
                        throttleAim = GetCurveMaxSpeed(pieceArray[curve].Radius.Value);
                    /*}
                    if (currentSpeed > highestSpeed)
                    {
                        highestSpeed = currentSpeed;
                    }*/
                    if (pieceAccum[currentPiece] + currentPieceLocation >= pieceAccum[curve] - Sum(13,0))
                    {
                        if ((currentSpeed / 10) >= throttleAim)
                        {
                            sendThrottle = 0f;
                        }
                        else
                        {
                            sendThrottle = throttleAim;
                        }
                    }
                    else
                    {
                        sendThrottle = 1f;
                    }
                    Console.WriteLine("Current Throttle is " + sendThrottle.ToString());
                    Console.WriteLine("Total times crashed is " + crashCount);
                    Console.WriteLine("Speed in units per tick is " + currentSpeed);
                    if (turbo)
                    {

                        Console.WriteLine("Turbo is " + turboDuration + " in milliseconds, " + turboTicks + " in ticks. It multiplies by " + turboMultiplier);
                        if (currentPiece > GetNextCurve(currentPiece))
                        {
                            if (!msgSent)
                            {
                                msgSent = true;
                                send(new Turbo("Woo"));
                                turbo = false;
                            }
                        
                        }
                    }
                    if (!msgSent)
                    {
                        msgSent = true;
                        send(new Throttle(sendThrottle));
                    }
					break;
				case "join":
					Console.WriteLine("Joined");
					send(new Ping());
					break;
				case "gameInit":
					Console.WriteLine("Race init");
                    GetPieces(msg);
                    GetLanes(msg);
                    pieceAccum = GetPieceDistances(pieceArray);
                    generalDirection = GetDirectionToAim(pieceArray);
                    nextSeg = GetSubTrackList(0);
                    Console.WriteLine(generalDirection.ToString());
                    curve = GetNextCurve(0);
					send(new Ping());
					break;
				case "gameEnd":
					Console.WriteLine("Race ended");
					send(new Ping());
					break;
				case "gameStart":
					Console.WriteLine("Race starts");
					send(new Ping());
					break;
                case "crash":
                    this.gameTick++;
                    crashCount++;
                    break;
                case "spawn":
                    this.gameTick++;
                    break;
                case "lapFinished":
                    this.gameTick++;
                    lapCount++;
                    break;
                case "turboAvailable":
                    GetTurboInfo(msg);
                    break;
				default:
					send(new Ping());
					break;
			}
		}
	}

	private void send(SendMsg msg) {
		writer.WriteLine(msg.ToJson());
	}

    private Direction GetDirectionToAim(List<Piece> parts)
    {
        int total = 0;
        foreach (Piece part in parts)
        {
            if (part.Angle.HasValue)
            {
                if (part.Angle.Value > 0)
                {
                    total++;
                }
                else
                {
                    total--;
                }
            }
        }
        if (total >= 0)
        {
            return Direction.Right;
        }
        else
        {
            return Direction.Left;
        }
    }

    private Direction GetPieceDirection(Piece part)
    {
        if (part.Angle.HasValue)
        {
            if (part.Angle > 0)
            {
                return Direction.Right;
            }
            else
            {
                return Direction.Left;
            }
        }
        return this.generalDirection;
    }

    private void GetPieces(MsgWrapper msg)
    {
        pieceArray = new List<Piece>();
        jsonReader = new JsonTextReader(new StringReader(msg.data.ToString()));
        while (jsonReader.Read())
        {
            if (jsonReader.Value != null)
            {
                if (jsonReader.Value.ToString() == "pieces")
                {
                    int pieces = 0;
                    Console.WriteLine("Found it");
                    while (jsonReader.Read() && jsonReader.TokenType.ToString() != "EndArray")
                    {
                        if (jsonReader.TokenType.ToString() == "StartObject")
                        {
                            Console.WriteLine("New Piece");
                            Piece tempPiece = new Piece(pieces);
                            while (jsonReader.TokenType.ToString() != "EndObject")
                            {
                                Console.WriteLine(jsonReader.TokenType.ToString());
                                if (jsonReader.Value != null)
                                {

                                    switch (jsonReader.Value.ToString())
                                    {
                                        case "length":
                                            {
                                                jsonReader.Read();
                                                Console.WriteLine("The stored Length is " + jsonReader.Value);
                                                tempPiece.Length = (float)Convert.ToDouble(jsonReader.Value.ToString());

                                                break;
                                            }
                                        case "radius":
                                            {
                                                jsonReader.Read();
                                                tempPiece.Radius = (float)Convert.ToDouble(jsonReader.Value.ToString());
                                                Console.WriteLine("The stored Radius is " + jsonReader.Value);
                                                break;
                                            }
                                        case "angle":
                                            {
                                                jsonReader.Read();
                                                tempPiece.Angle = (float)Convert.ToDouble(jsonReader.Value.ToString());
                                                Console.WriteLine("The stored Angle is " + jsonReader.Value);
                                                break;
                                            }
                                        case "switch":
                                            {
                                                jsonReader.Read();
                                                tempPiece.LaneSwitch = Convert.ToBoolean(jsonReader.Value.ToString());
                                                Console.WriteLine("The stored Bool is " + jsonReader.Value);
                                                break;
                                            }
                                        default:

                                            Console.WriteLine("Not identified " + jsonReader.Value.ToString()); break;

                                    }
                                }
                                jsonReader.Read();
                            }
                            pieceArray.Add(new Piece(pieces, tempPiece.LaneSwitch, tempPiece.Length, tempPiece.Angle, tempPiece.Radius));
                            Console.WriteLine("Piece Ended");
                            pieces++;
                        }
                    }
                    Console.WriteLine("Array Completed");
                    continue;
                }

                if (jsonReader.Value.ToString() == "laps")
                {
                    jsonReader.Read();
                    lapNum = Convert.ToInt32(jsonReader.Value.ToString());
                    Console.WriteLine("The stored lap count is " + jsonReader.Value);
                }
            }
        }
    }

    private float[] GetPieceDistances(List<Piece> list)
    {
        float[] returnArray = new float[list.Count];
        float count = 0;
        for (int i = 0; i < list.Count; ++i )
        {
            if (list[i].Length.HasValue)
            {
                count += list[i].Length.Value;
            }
            else
            {
                //(angle � pi/180) � r
                count += (float)(((double)(Math.Abs((decimal)list[i].Angle.Value)) * (Math.PI / 180)) * list[i].Radius.Value);
            }
            returnArray[i] = count;
        }
        return returnArray;
    }

    private void GetLanes(MsgWrapper msg)
    {
        numLanes = 0;

        jsonReader = new JsonTextReader(new StringReader(msg.data.ToString()));
        while (jsonReader.Read())
        {
            if (jsonReader.Value != null && jsonReader.Value.ToString() == "index")
            {
                numLanes++;
            }
        }
        numLanes--;
    }

    private int GetLaneToAim(Direction direction)
    {
        if (direction == Direction.Right)
        {
            return this.numLanes;
        }
        return 0;
    }

    private void GetCarInfo(MsgWrapper msg)
    {
        jsonReader = new JsonTextReader(new StringReader(msg.data.ToString()));

        while (jsonReader.Read())
        {
            if (jsonReader.Value != null)
            {
                switch (jsonReader.Value.ToString())
                {
                    case "angle":
                        {
                            jsonReader.Read();
                            currentAngle = (float)Convert.ToDouble(jsonReader.Value.ToString());
                            break;
                        }
                        case "pieceIndex":
                        {
                            jsonReader.Read();
                            previousPiece = currentPiece;
                            currentPiece = Convert.ToInt32(jsonReader.Value.ToString());
                            break;
                        }
                    case "inPieceDistance":
                        {
                            jsonReader.Read();
                            previousPieceLocation = currentPieceLocation;
                            currentPieceLocation = (float)Convert.ToDouble(jsonReader.Value.ToString());
                            currentSpeed = (pieceAccum[currentPiece] + currentPieceLocation) - (pieceAccum[previousPiece] + previousPieceLocation);
                            break;
                        }
                    case "startLaneIndex":
                        {
                            jsonReader.Read();
                            previousLane = Convert.ToInt32(jsonReader.Value.ToString());
                            break;
                        }
                    case "endLaneIndex":
                        {
                            jsonReader.Read();
                            currentLane = Convert.ToInt32(jsonReader.Value.ToString());
                            break;
                        }

                }
            }
        }
    }

    private void GetTurboInfo(MsgWrapper msg)
    { 
        jsonReader = new JsonTextReader(new StringReader(msg.data.ToString()));
        turbo = true;
        jsonReader.Read();
        jsonReader.Read();
        jsonReader.Read();
        turboDuration = (float)Convert.ToDouble(jsonReader.Value.ToString());
        jsonReader.Read();
        jsonReader.Read();
        turboTicks = Convert.ToInt32(jsonReader.Value.ToString());
        jsonReader.Read();
        jsonReader.Read();
        turboMultiplier = (float)Convert.ToDouble(jsonReader.Value.ToString());
    }

    private List<Piece> GetSubTrackList(int currentPos)
    {
        int nextSwitch = FindSwitchAfter(currentPos);
        Console.WriteLine(nextSwitch);
        int afterSwitch = FindSwitchAfter(nextSwitch);
        Console.WriteLine(afterSwitch);
        if (NumWrapper.RoundNumber(afterSwitch - nextSwitch, pieceArray.Count) > pieceArray.Count - nextSwitch)
        {
            List<Piece> tempList = new List<Piece>();
            tempList.AddRange(pieceArray.GetRange(nextSwitch, pieceArray.Count - nextSwitch));
            tempList.AddRange(pieceArray.GetRange(0, afterSwitch));
            return tempList;
 
        }
        return pieceArray.GetRange(nextSwitch, afterSwitch - nextSwitch);
    }

    private int GetNextCurve(int pos)
    {
        for (int i = pos; i < pieceArray.Count; i++)
        {
            if (pieceArray[i].Index > pos && pieceArray[i].Radius.HasValue)
            {
                return pieceArray[i].Index;
            }
        }
        for (int i = 0; i < pos; i++)
        {
            if (pieceArray[i].Index < pos && pieceArray[i].Radius.HasValue)
            {
                return pieceArray[i].Index;
            }
        }
        throw new Exception("Curve Not Found");
    }

    private int FindSwitchAfter(int pos)
    {
        for (int i = pos; i < pieceArray.Count; i++)
        {
            if (pieceArray[i].Index > pos && pieceArray[i].LaneSwitch)
            {
                return pieceArray[i].Index;
            }
        }
        for (int i = 0; i <= pos; i++)
        {
            if (pieceArray[i].Index < pos && pieceArray[i].LaneSwitch)
            {
                return pieceArray[i].Index;
            }
        }
        throw new Exception("No switch found");

    }

    private float GetCurveMaxSpeed(float radius)
    {
        if (radius > 101)
        {
            return .92f;
        }
        else if (radius < 100)
        {
            return .45f;
        }
        else
        {
            return .64f;
        }
    }

    private float GetBrakeDistance(float currentSpeed, float speedAim)
    {
        float total = 0;
        for (float i = currentSpeed; i / 10 > speedAim; i -= 1f)
        {
            total += i;
        }
        return total;
    }

    private float Sum(float num, float total)
    {
        if (num > 0)
        {
            return Sum(--num, (total += num));
        }
        return total;
    }


}

static class NumWrapper
{
    public static int RoundNumber(int num, int maxNum)
    {
        if (num > maxNum)
        {
            return RoundNumber(num - maxNum, maxNum);
        }
        if (num < 0)
        {
            return RoundNumber(maxNum + num, maxNum);
        }
        return num;
    }

}

class MsgWrapper {
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data) {
    	this.msgType = msgType;
    	this.data = data;
    }
}

abstract class SendMsg {
	public string ToJson() {
		return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
	}
	protected virtual Object MsgData() {
        return this;
    }

    protected abstract string MsgType();
}

class Join: SendMsg {
	public string name;
	public string key;
	public string color;

	public Join(string name, string key) {
		this.name = name;
		this.key = key;
		this.color = "black";
	}

	protected override string MsgType() { 
		return "join";
	}
}

class JoinServer : SendMsg
{
        public Join botId;
        public string trackName;
        public string password;
        public int carCount;

    public JoinServer(string trackName, string password, int? carCount, string name, string key)
    {
        botId = new Join(name, key);
        if (password != null)
        {
            this.password = password;
        }
        else
        {
            this.password = "";
        }
        if (carCount.HasValue)
        {
            this.carCount = carCount.Value;
        }
        else
        {
            this.carCount = 1;
        }
        this.trackName = trackName;
    }

    protected override object MsgData()
    {
        return this;
    }

    protected override string MsgType()
    {
        return "joinRace";
    }
}

class CreateRace : JoinServer
{
    public CreateRace(string trackName, string password, int? carCount, string name, string key)
        : base(trackName, password, carCount, name, key)
    {
    }
    protected override object MsgData()
    {
        return base.MsgData();
    }

    protected override string MsgType()
    {
        return "createRace";
    }
}

class Ping: SendMsg {
	protected override string MsgType() {
		return "ping";
	}
}

class Throttle: SendMsg {
	public double value;

	public Throttle(double value) {
		this.value = value;
	}

	protected override Object MsgData() {
        return value;
	}

	protected override string MsgType() {
		return "throttle";
	}
}

class Turbo : SendMsg
{
    public string msg;

    public Turbo(string msg)
    {
        this.msg = msg;
    }

    protected override Object MsgData()
    {
        return this.msg;
    }

    protected override string MsgType()
    {
        return "turbo";
    }
}

class SwitchLane : SendMsg
{
    public Direction value;

    public SwitchLane(Direction value)
    {
        this.value = value;
    }

    protected override Object MsgData()
    {
        if (value == Direction.Right)
        {
            return "Right";
        }
        else
        {
            return "Left";
        }
    }

    protected override string MsgType()
    {
        return "switchLane";
    }
}

public enum Direction
{
    Right = 100,
    Left = -100,
}

public class Piece
{
    int index;
    public int Index
    {
        get
        {
            return index;
        }
    }
    float? length;
    public float? Length
    {
        get
        {
            return length;
        }
        set
        {
            length = value;
        }
    }
    bool laneSwitch;
    public bool LaneSwitch
    {
        get
        {
            return laneSwitch;
        }
        set
        {
            laneSwitch = value;
        }
    }
    float? angle;
    public float? Angle
    {
        get
        {
            return angle;
        }
        set
        {
            angle = value;
        }
    }
    float? radius;
    public float? Radius
    {
        get
        {
            return radius;
        }
        set
        {
            radius = value;
        }
    }

    public Piece(int index)
    {
        this.index = index;
        this.laneSwitch = false;
        this.length = null;
        this.angle = null;
        this.radius = null;
    }
    public Piece(int index, bool laneSwitch, float? length, float? angle, float? radius)
    {
        this.index = index;
        this.laneSwitch = laneSwitch;
        this.length = length;
        this.angle = angle;
        this.radius = radius;
    }
}